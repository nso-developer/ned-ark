NSO_IMAGE=cisco-nso-dev:5.2

#ifeq ($(NSO_VERSION),)
#$(error You must set the NSO_VERSION variable)
#endif

.PHONY: update-github-yangmodels update-gitlab-ned-repo-template common-init common-build-and-test

github-yangmodels:
	git clone git@github.com:YangModels/yang.git github-yangmodels


update-github-yangmodels: github-yangmodels
	cd github-yangmodels #; git pull

gitlab-ned-repo-template:
	git clone git@gitlab.com:nso-developer/gitlab-ned-repo-template.git

update-gitlab-ned-repo-template: gitlab-ned-repo-template
	cd gitlab-ned-repo-template; git pull


common-init:
	@echo "Building a NED; $(NED)"
	if [ ! -d $(NED) ]; then echo "$(NED) does not exist, creating..."; cp -a gitlab-ned-repo-template $(NED); rm -rf $(NED)/.git; cd $(NED); git init; git remote add origin git@gitlab.com:nso-developer/$(NED).git; fi
	rm -rf ./$(NED)/$(NED)
	docker run -t --rm -v $$(pwd):/src $(NSO_IMAGE) bash -lc "cd /src/$(NED); ncs-make-package --netconf-ned /src/github-yangmodels/$(YMPATH) $(NED); chown -R 1000:1000 $(NED)"
	@echo "-- Rewrite minimum ncs-version"
	sed -i -e 's/<ncs-min-version>.*<\/ncs-min-version>/<ncs-min-version>4.5<\/ncs-min-version>/' $(NED)/$(NED)/src/package-meta-data.xml.in
	@echo "-- Removing ned-id line, must not be in NSO 4 and NSO 5 will fill it in anyway"
	sed -i -e '/<ned-id>/d' $(NED)/$(NED)/src/package-meta-data.xml.in
	@echo "-- Fill in ned-id in test/add-device.xml TODO: dynamically read out version number from package-meta-data.xml"
	sed -i -e 's/<ned-id.*/<ned-id xmlns:$(NED)-nc-1.0="http:\/\/tail-f.com\/ns\/ned-id\/$(NED)-nc-1.0">$(NED)-nc-1.0:$(NED)-nc-1.0<\/ned-id>/' $(NED)/test/add-device.xml
	@echo "-- Remove capabilities and other .xml crap files"
	rm $(NED)/$(NED)/netsim/*.xml
	@echo "-- Place template README"
	cp template/README.org $(NED)/README.org

common-build-and-test:
	@echo "-- Building NED"
	cd $(NED); $(MAKE) NSO_VERSION=$(NSO_VERSION) build
	-@grep TODO $(NED)/test/device-config-hostname.xml && echo "-- You need to edit the configuration in $(NED)/test/device-config-hostname.xml so that it actually works on this device. I'm waiting. Press enter when done" && read DUMMY
	@echo "-- Running tests, which should pass if you correctly modified the config"
	cd $(NED); $(MAKE) NSO_VERSION=$(NSO_VERSION) test stop clean
	@echo "-- Ok, that wen't well, now git commit and push"


# -- NEDs

# broken :/
ned-iosxr-613: update-github-yangmodels gitlab-ned-repo-template
	$(MAKE) NED=$@ YMPATH=vendor/cisco/xr/613 common-init
	@echo "-- Remove OpenConfig models because the NED won't compile with those"
	rm -f $@/$@/src/yang/*openconfig*
	@echo "-- Remove as it contains openconfig reference"
	rm -f $@/$@/src/yang/cisco-xr-routing-policy-deviations.yang
	$(MAKE) NED=$@ common-build-and-test


ned-iosxr-621: update-github-yangmodels gitlab-ned-repo-template
	$(MAKE) NED=$@ YMPATH=vendor/cisco/xr/621 common-init
	@echo "-- Remove OpenConfig models because the NED won't compile with those"
	rm -f $@/$@/src/yang/*openconfig*
	@echo "-- Use prepared config"
	cp props/xr5-device-config-hostname.xml $@/test/device-config-hostname.xml
	$(MAKE) NED=$@ common-build-and-test


ned-iosxr-662: update-github-yangmodels gitlab-ned-repo-template
	$(MAKE) NED=$@ YMPATH=vendor/cisco/xr/662 common-init
	@echo "-- Remove OpenConfig models because the NED won't compile with those"
	rm -f $@/$@/src/yang/*openconfig*
	@echo "-- Remove XR NACM deviations as Cisco botched this - in addition to the deviation they modified the original YANG model and removed the leaf that is being deviated (e.g. 'enable-acm'). Not allowed. Bad Cisco."
	rm $@/$@/src/yang/cisco-xr-ietf-netconf-acm-deviations.yang
	rm $@/$@/src/yang/cisco-xr-ietf-netconf-monitoring-deviations.yang
	@echo "-- Remove sysadmin-hw-module-xrv9k - prefix collision for name 'calvados_hw_module' - two YANG models with same prefix, not sure which is correct. One is for xrv9k, is the other perhaps for a physical box and you are really only supposed to get support for one?"
	rm $@/$@/src/yang/Cisco-IOS-XR-sysadmin-hw-module-xrv9k.yang
	@echo "-- Remove sysadmin-asr9k-envmon-ui - same reason as above"
	rm $@/$@/src/yang/Cisco-IOS-XR-sysadmin-asr9k-envmon-ui.yang
	rm $@/$@/src/yang/Cisco-IOS-XR-sysadmin-asr9k-envmon-types.yang
	@echo "-- Add hack for getting netsim to start"
	cp props/netsim-iosxr-vm-hack.xml $@/$@/netsim/
	@echo "-- Use prepared config"
	cp props/xr6-device-config-hostname.xml $@/test/device-config-hostname.xml
	$(MAKE) NED=$@ common-build-and-test
