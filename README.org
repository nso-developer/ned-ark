* Installation 00 - The NED Ark
  Construct NEDs, primarily based on the YangModels repo!

  Compiling NETCONF NEDs out of YANG models for Cisco NSO is in theory simple and straight forward. Reality is often different. Models won't compile cleanly due to incorrectness - they're simply not compliant to the YANG RFC. However, sometimes it's possible to work around by applying fixes. This repo aims to structure this work by keeping the recipe for building a NED, including said fixes, in git.

  Building a NED thus consists of running a make target, for example ~ned-iosxr-621~. This target will in turn invoke common parts, like the initialization of a directory and copying of files from the YangModels repo. Then it will apply workarounds for this particular NED / set of YANG models and finally invoke a common build and test step. The template for what a NED repo should look like, in terms of Makefile, Dockerfile etc is brought in from the ~gitlab-ned-repo-template~ repo.

** Etymology
   Installation 00 is a Halo reference. It is a Forerunner installation that in turn is producing other installations, a.k.a. the Halo rings.

   Similar to how Installation 00 produces Halo rings. This repo will produce NEDs.
